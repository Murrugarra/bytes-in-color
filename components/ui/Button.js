function Button(props) {
  return <a className="btn btn-success" onClick={props.onClick}>
    {props.children}
  </a>
}

export default Button;