import CardItem from "./CardItem";
import items from "../../data/itemsData";

function CardList() {
  return (
    <div className="container">
      <div className="row">
        {items.map((item) => (
          <div
            key={`card${item.itemId}`}
            className="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-2 col-xl-2 col-xxl-4"
          >
            <CardItem
              itemId={item.itemId}
              imageUrl={item.itemImageUrl}
            />
          </div>
        ))}
      </div>
    </div>
  );
}

export default CardList;
