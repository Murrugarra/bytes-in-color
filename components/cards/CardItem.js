import Image from "next/image";
import Link from "next/link";
import classes from "../../styles/CardItem.module.css";

function CardItem({ itemId, imageUrl }) {
  return (
    <div className={`${classes.CardItem} card`}>
      <Image
        src={imageUrl}
        alt="card-design"
        className="card-img-top"
        height={220}
        width={150}
      />
      <div className="card-body">
        <div className="d-grid gap-2">
          <Link
            href={{
              pathname: `/marketplace/item/[itemId]`,
              query: { itemId: itemId },
            }}
            passHref
          >
            <button className="btn btn-dark" type="button">
              Buy
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default CardItem;
