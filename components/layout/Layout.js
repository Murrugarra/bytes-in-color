import NavigationBar from "./NavigationBar";

function Layout(props) {
  return (
    <div>
      <NavigationBar />
      <main className="container">{props.children}</main>
    </div>
  );
}

export default Layout;
