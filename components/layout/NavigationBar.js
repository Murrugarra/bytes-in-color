import Link from "next/link";
import classes from "../../styles/NavigationBar.module.css";

function NavigationBar() {
  const collapseMenuClassName = "collapse navbar-collapse";

  function sayHello() {
    alert("Hi!");
  }

  return (
    <nav className={`${classes.NavigationBar} navbar navbar-dark bg-dark`}>
      <div className="container">
        <Link href="/">
          <a className="navbar-brand">
            <b>Bytes</b>InColor
          </a>
        </Link>
        <div>
          <Link href="/marketplace/item/new" passHref>
            <button className="btn btn-success">Create</button>
          </Link>
          &nbsp;
          <button className="btn btn-primary" onClick={sayHello}>
            Connect to Wallet
          </button>
        </div>
      </div>
    </nav>
  );
}

export default NavigationBar;
