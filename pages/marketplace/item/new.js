import Layout from "../../../components/layout/Layout";

function NewArticle() {
  return (
    <Layout>
      <h1>New Article</h1>
    </Layout>
  );
}

export default NewArticle;
