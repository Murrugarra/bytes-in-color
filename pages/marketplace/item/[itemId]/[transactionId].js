import { useRouter } from "next/router";
import Layout from "../../../../components/layout/Layout";

function ItemTransaction() {
  const router = useRouter();
  const { itemId, transactionId } = router.query;

  return (
    <Layout>
      <h1>
        Transactionn Item: {itemId}, Transaction: {transactionId}{" "}
      </h1>
    </Layout>
  );
}

export default ItemTransaction;
