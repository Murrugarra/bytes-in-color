import { useRouter } from "next/router";
import Layout from "../../../../components/layout/Layout";
import { getItemById } from "../../../../data/itemsData";
import Image from "next/image";
import Button from "../../../../components/ui/Button";
import { useEffect, useState } from "react";

function ArtDetail() {
  const router = useRouter();
  const { itemId } = router.query;
  const [itemInfo, setItemInfo] = useState(null);

  useEffect(() => {
    setItemInfo(getItemById(itemId));
  }, [itemId]);

  const buyItem = () => {
    alert("Will Buy!");
  };

  if (itemInfo == null) return <h1>Loading...</h1>;

  return (
    <Layout>
      <p>
        <h1>{itemInfo.title}</h1>
        {itemInfo.description}
      </p>
      <Image
        src={itemInfo.itemImageUrl}
        alt="card-design"
        className="card-img-top"
        height="100%"
        width="100%"
      />
      <Button onClick={buyItem}>GOLA</Button>
    </Layout>
  );
}

export default ArtDetail;
