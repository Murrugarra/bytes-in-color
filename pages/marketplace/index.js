import CardList from "../../components/cards/CardList";
import Layout from "../../components/layout/Layout";

export default function Home() {
  return (
    <Layout>
      <CardList />
    </Layout>
  );
}
