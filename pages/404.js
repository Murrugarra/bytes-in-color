import Layout from "../components/layout/Layout";

function NotFoundPage() {
  return (
    <Layout>
      <h1>Not Found</h1>
    </Layout>
  );
}

export default NotFoundPage;
