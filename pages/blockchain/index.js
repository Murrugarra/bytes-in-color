import { useState, useEffect } from "react";
import web3 from "../../contracts/web3Adapter";
import contract from "../../contracts/itemContract";

function BlockchainPage(props) {
  const [balance, setBalance] = useState(0);
  const [manager, setManager] = useState(null);
  const [executingTransaction, setExecutingTransaction] = useState(false);

  useEffect(() => {
    async function retrieveManager() {
      const manager = await contract.methods.manager().call();
      const balance = await web3.eth.getBalance(contract.options.address);
      setManager(manager);
      setBalance(balance);
    }

    retrieveManager();
  }, []);

  const executeTransactionInContract = async () => {
    if (executingTransaction) {
      console.warn("transaction in progress.");
      return;
    }

    setExecutingTransaction(true);
    // transaction begins

    const currentAccount = window.ethereum.selectedAddress;

    // debugger;
    const transaction = {
      from: currentAccount,
      value: web3.utils.toWei("1", "ether"),
    };

    await contract.methods.purchaseItem("S3").send(transaction);

    // transaction finish
    setExecutingTransaction(false);
  };

  return (
    <div>
      <h1>Blockchain Junta</h1>
      <p>Balance: {balance}.</p>
      {manager && <p>Manager: {manager}.</p>}
      <hr />
      <button onClick={executeTransactionInContract}>
        {executingTransaction ? "Executing..." : "Submit 1 ETH"}
      </button>
    </div>
  );
}

export default BlockchainPage;
