import Link from "next/link";
import Layout from "../components/layout/Layout";

export default function Home() {
  return (
    <Layout>
      <h1>Welcome to The Best NFT Marketplace in the world!</h1>
      <p>
        <Link href="/marketplace" passHref>
          <button className="btn btn-dark">Ir Al Marketplace</button>
        </Link>
      </p>
    </Layout>
  );
}
