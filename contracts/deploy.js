const HDWalletProvider = require("truffle-hdwallet-provider");
const Web3 = require("web3");
const Contract = require("./compile.js");
const Config = require("./config");

const provider = new HDWalletProvider(Config.B_PHRASE, Config.B_NETWORK_URI);

const web3 = new Web3(provider);

const deploy = async () => {
  try {
    const accounts = await web3.eth.getAccounts();
    const transactionAccount = accounts[0];

    console.log(
      "Deploying contract with address: " +
        transactionAccount +
        ", to " +
        Config.B_NAME +
        "network."
    );

    const result = await new web3.eth.Contract(JSON.parse(Contract.interface))
      .deploy({ data: "0x" + Contract.bytecode })
      .send({ gas: "2000000", from: transactionAccount });

    console.log("Contract deployed to address: " + result.options.address);
  } catch (error) {
    console.error(error);
  }

  process.exit(0);
};

deploy();
