const Web3 = require("web3");
const Config = require("./config");

let provider;
let providerSource;

if (typeof window !== "undefined" && typeof window.web3 !== "undefined") {
  provider = window.web3.currentProvider;
  providerSource = "Browser";
} else {
  provider = new Web3.providers.HttpProvider(Config.B_NETWORK_URI);
  providerSource = "Cloud Node";
}

console.log(providerSource);

module.exports = new Web3(provider);
