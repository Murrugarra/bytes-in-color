const web3 = require("./web3Adapter");
const abi = require("./target/ItemSmartContractABI.json");

const address = "0xaf950af10577FEd877d63Ba3d7bF31cEb21BcE26";
const contract = new web3.eth.Contract(abi, address);

module.exports = contract;
