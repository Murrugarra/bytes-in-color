// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.4.0;

contract ItemSmartContract {

    address public manager;
    
    mapping(address => string) public itemsPurchased;
    
    constructor () public {
        manager = msg.sender;
    }

    function purchaseItem(string itemId) public payable {
        // Add logic to avoid to addresses to buy the same article
        // Always verify that only buy once
        require(msg.value == 1 ether, "Amount should be 1 ether");
        itemsPurchased[msg.sender] = itemId;
    }
    
    function transfer(address somebody) public {
        require(msg.sender == manager, "Cuevita ctmr");
        somebody.transfer(address(this).balance);
    }
    
    function sal() public view returns(uint256) {
        require(msg.sender == manager, "Cuevita ctmr");
        return address(this).balance;
    }

}