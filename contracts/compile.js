const path = require("path");
const fs = require("fs");
const solc = require("solc");

const smartContractPath = path.resolve(
  __dirname,
  "src",
  "ItemSmartContract.sol"
);
const source = fs.readFileSync(smartContractPath, "utf8");

const compiled = solc.compile(source);
const compiledContract = compiled.contracts[":ItemSmartContract"];

const abiContent = compiledContract.interface;

fs.writeFile(
  path.resolve(__dirname, "target", "ItemSmartContractABI.json"),
  abiContent,
  () => {
    console.log("ABI Created with content: " + abiContent);
  }
);

module.exports = compiledContract;
